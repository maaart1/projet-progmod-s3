package model.lecture_ply;

public class NotPlyFileException extends Exception{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    
    public NotPlyFileException(String namefile){
        super("Le fichier n'est pas un fichier ply " + namefile);
    }
    public NotPlyFileException(){
        super("Le fichier n'est pas un fichier ply ");
    }
}
