package model.lecture_ply;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;

import exeption.ColorReadException;
import exeption.FacesArrayIsFullException;
import model.model3D.Model3D;
import model.model3D.polygone.Face;
import model.model3D.polygone.Point;
/**
 * Classe "Read" : permet la lecture des fichiers ply, vérifier si le fichier
 * est bien rédiger.
 * 
 * @author Martin Thibaut, Léonard Corre, Hugo Dutoit
 */
public class Read {

	protected BufferedReader sc;
	protected int nbFaces;
	protected int nbPoints;

	public Read() {

	}
	/**
	 * Méthode permettant de vérifier l'entête du fichier ply.
	 * 
	 * @param sc
	 * @return Un boolean si true ou false l'entête est correct.
	 */
	public boolean isHeaderOk(BufferedReader sc) throws IOException, NotPlyFileException {

		String actual = sc.readLine();
		if (!actual.equals("ply")) {
			throw new NotPlyFileException();
		}
		actual = sc.readLine();
		if (!actual.contains("format")) {
			throw new NotPlyFileException();
		}
		actual = sc.readLine();
		while (actual.contains("comment")) {
			actual = sc.readLine();
		}
		if (actual.contains("element vertex")) {
			nbPoints = Integer.parseInt(actual.split(" ")[2]);
			actual = sc.readLine();
		}

		while (actual.contains("property") || !actual.contains("end_header")) {
			if (actual.contains("element")) {
				nbFaces = Integer.parseInt(actual.split(" ")[2]);
			}
			actual = sc.readLine();
		}

		return true;
	}
	/**
	 * Méthode permettant d'obtenir l'auteur du fichier ply s'il y en a un.
	 * 
	 * @param f
	 * @return Un string contenant l'auteur du fichier.
	 */
	public String getAuteur(File f) throws IOException {
		String auteur = "inconnu";
		try (BufferedReader br = new BufferedReader(new FileReader(f));) {
			String ligne = br.readLine();
			while (!ligne.contains("comment") && !ligne.contains("end_header")) {
				ligne = br.readLine();
				if (ligne.contains("comment made by")) {
					auteur = ligne.substring("comment made by".length());
				}
			}
		} catch (IOException e) {
			throw new IOException();
		}
		return auteur;
	}
	/**
	 * Retourne le nombre de points du fichier
	 * 
	 * @return Un Integer, qui correspond au nombres de points du Model3D
	 */
	public int getNbPoints() {
		return this.nbPoints;
	}
	/**
	 * Retourne le nombre de faces du fichier.
	 * 
	 * @return Un Integer, qui correspond au nombres de faces du Model3D.
	 */
	public int getNbFaces() {
		return this.nbFaces;
	}
	/**
	 * Factory de Model3D.
	 * 
	 * @param f
	 * @return Un Model3D, qui correspond au fichier ply.
	 */
	public Model3D ui(String f) throws IOException, NotPlyFileException, NumberFormatException,
			FacesArrayIsFullException, ColorReadException {
		if (f == null)
			throw new NullPointerException();
		sc = new BufferedReader(new StringReader(f));
		try {
			isHeaderOk(sc);
		} catch (NotPlyFileException e) {
			throw new NotPlyFileException(f);
		}

		int som = getNbPoints();
		int nbface = getNbFaces();
		Point[] points = new Point[som];
		Model3D model = new Model3D(nbface);
		String[] tmp1 = null;
		String buffer;

		for (int i = 0; i < som; i++) {
			try {
				buffer = sc.readLine();
				tmp1 = buffer.split(" ");
			} catch (IOException e) {
				e.printStackTrace();
			}
			points[i] = (new Point(Double.parseDouble(tmp1[0]), Double.parseDouble(tmp1[1]),
					Double.parseDouble(tmp1[2])));
		}

		for (int i = 0; i < nbface; i++) {
			try {
				buffer = sc.readLine();
				tmp1 = buffer.split(" ");

			} catch (IOException e1) {
				throw new ColorReadException();
			}

			model.addFace(new Face(Integer.parseInt(tmp1[0])));
			if (tmp1.length > (Integer.parseInt(tmp1[0]) + 1)) {
				model.setFaceColor(i, new int[] { Integer.parseInt(tmp1[tmp1.length - 3]),
						Integer.parseInt(tmp1[tmp1.length - 2]), Integer.parseInt(tmp1[tmp1.length - 1]) });
			}

			for (int x = 0; x < Integer.parseInt(tmp1[0]); x++) {
				model.addPoint(points[Integer.parseInt(tmp1[x + 1])], i);
			}
		}

		try {
			sc.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return model;
	}

	public Model3D ui(File f) throws IOException, NotPlyFileException, NumberFormatException, FacesArrayIsFullException,
			ColorReadException {
		if (f == null)
			throw new NullPointerException();
		try {
			sc = new BufferedReader(new FileReader(f));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			isHeaderOk(sc);
		} catch (NotPlyFileException e) {
			throw new NotPlyFileException(f.getName());
		}

		int som = getNbPoints();
		int nbface = getNbFaces();
		Point[] points = new Point[som];
		Model3D model = new Model3D(nbface);
		String[] tmp1 = null;
		String buffer;

		for (int i = 0; i < som; i++) {
			try {
				buffer = sc.readLine();
				tmp1 = buffer.split(" ");
			} catch (IOException e) {
				e.printStackTrace();
			}
			points[i] = (new Point(Double.parseDouble(tmp1[0]), Double.parseDouble(tmp1[1]),
					Double.parseDouble(tmp1[2])));
		}

		for (int i = 0; i < nbface; i++) {
			try {
				buffer = sc.readLine();
				tmp1 = buffer.split(" ");

			} catch (IOException e1) {
				throw new ColorReadException();
			}

			model.addFace(new Face(Integer.parseInt(tmp1[0])));
			if (tmp1.length > (Integer.parseInt(tmp1[0]) + 1)) {
				model.setFaceColor(i, new int[] { Integer.parseInt(tmp1[tmp1.length - 3]),
						Integer.parseInt(tmp1[tmp1.length - 2]), Integer.parseInt(tmp1[tmp1.length - 1]) });
			}

			for (int x = 0; x < Integer.parseInt(tmp1[0]); x++) {
				model.addPoint(points[Integer.parseInt(tmp1[x + 1])], i);
			}
		}

		try {
			sc.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return model;
	}
}
