package model.model3D;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import exeption.FacesArrayIsFullException;
import misc.MOS.Observer;
import misc.MOS.AbstractSubject;
import model.model3D.polygone.Face;
import model.model3D.polygone.Point;

/**
 * Classe "Model3D" : constitue un Model3D, correspondant à un fichier ply.
 * 
 * @author Martin Thibaut, Léonard Corre, Hugo Dutoit
 */

public class Model3D extends AbstractSubject {
    // ATTRIBUTES
    private Face[] faces;
    public List<Point> points;
    private int sommet = 0;
    public double xOffset;
    public double yOffset;
    private static final int PAIR = 2;

    // CONSTRUCTOR
    /**
     * Contructeur de Model3D.
     * 
     * @param length
     * @param controller
     */
    public Model3D(int length, Observer controller) {
        this.faces = new Face[length];
        this.points = new ArrayList<>();
        this.attach(controller);
    }

    /**
     * Constructeur de Mode3D.
     * 
     * @param length
     */
    public Model3D(int length) {
        this.faces = new Face[length];
        this.points = new ArrayList<>();
    }

    /**
     * Méthode l'affichage des faces sous formes de coordonnées.
     * 
     * @return La liste des faces du Model3D.
     */
    public String toString() {
        String res = "";
        for (int i = 0; i < faces.length; i++) {
            res += this.faces[i] + "" + i + "\n";
        }
        return res;
    }

    // GETTERS /SETTERS
    /**
     * Retourne le tableaux de faces du Model3D.
     * 
     * @return Le tableau de faces.
     */
    public Face[] getFaces() {
        return this.faces;
    }

    public int getPointslength() {
        return this.points.size();
    }

    public int getFacesLength() {
        return this.faces.length;
    }

    // OTHER METHODE
    // OTHER METHODE
    /**
     * Ajoute une face au tableau de faces.
     * 
     * @param face
     * @throws FacesArrayIsFullException
     */
    public void addFace(Face face) throws FacesArrayIsFullException {
        if (this.isFull())
            throw new FacesArrayIsFullException();
        this.faces[this.sommet++] = face;
    }

    /**
     * Ajoute des face au tableau de faces.
     * 
     * @param faces
     * @throws FacesArrayIsFullException
     */
    public void addFace(Face[] faces) throws FacesArrayIsFullException {
        for (Face f : faces) {
            this.addFace(f);
        }
    }

    /**
     * Vérifie si le tableau de faces est plein.
     * 
     * @return True ou false, si le tableau est plein.
     */
    public boolean isFull() {
        return this.sommet == this.faces.length;
    }

    // sort Methode

    /**
     * Permet l'affichage des faces du Model3D dans l'ordre.
     */
    public void sortByAverageZ() {
        Arrays.sort(this.faces);

    }

    /**
     * Retourne la plus grande coordonnées X du Model3D.
     * 
     * @return Un double qui correspond à la plus grande valeur X du Model3D.
     */
    public double getMaxX() {
        if (this.getPointslength() == 0)
            return 0;
        double max = this.points.get(0).getX();
        double tmp;
        for (int i = 1; i < this.points.size(); i++) {
            tmp = this.points.get(i).getX();
            if (tmp > max) {
                max = tmp;
            }
        }
        return max;
    }

    /**
     * Retourne la plus petite coordonnées X du Model3D.
     * 
     * @return La plus petite valeur X du Model3D.
     */
    public double getMinX() {
        if (this.getPointslength() == 0)
            return 0;
        double min = this.points.get(0).getX();
        double tmp;
        for (int i = 1; i < this.points.size(); i++) {
            tmp = this.points.get(i).getX();
            if (tmp < min) {
                min = tmp;
            }
        }
        return min;
    }

    /**
     * Retourne la moyenne des coordonnées X du Model3D.
     * 
     * @return La moyenne des X du Model3D.
     */
    public double getCenterX() {
        double res = 0.0;
        for (Point p : this.points) {
            res += p.getX();
        }
        return res / (double) this.points.size();
    }

    /**
     * Retourne la plus grande coordonnées Y du Model3D.
     * 
     * @return La plus grande valeur Y du Model3D.
     */
    public double getMaxY() {
        if (this.getPointslength() == 0)
            return 0;
        double max = this.points.get(0).getY();
        double tmp;
        for (int i = 1; i < this.points.size(); i++) {
            tmp = this.points.get(i).getY();
            if (tmp > max) {
                max = tmp;
            }
        }
        return max;
    }

    /**
     * Retourne la plus petite coordonnées Y du Model3D.
     * 
     * @return La plus petite valeur Y du Model3D.
     */
    public double getMinY() {
        if (this.getPointslength() == 0)
            return 0;
        double min = this.points.get(0).getY();
        double tmp;
        for (int i = 1; i < this.points.size(); i++) {
            tmp = this.points.get(i).getY();
            if (tmp < min) {
                min = tmp;
            }
        }
        return min;
    }

    /**
     * Retourne la plus petite coordonnées Z du Model3D.
     * 
     * @return La plus petite valeur Z du Model3D.
     */
    public double getMaxZ() {
        if (this.getPointslength() == 0)
            return 0;
        double max = this.points.get(0).getZ();
        double tmp;
        for (int i = 1; i < this.points.size(); i++) {
            tmp = this.points.get(i).getZ();
            if (tmp > max) {
                max = tmp;
            }
        }
        return max;
    }

    /**
     * Retourne la plus petite coordonnées Z du Model3D.
     * 
     * @return La plus petite valeur Z du Model3D.
     */
    public double getMinZ() {
        if (this.getPointslength() == 0)
            return 0;
        double min = this.points.get(0).getZ();
        double tmp;
        for (int i = 1; i < this.points.size(); i++) {
            tmp = this.points.get(i).getZ();
            if (tmp < min) {
                min = tmp;
            }
        }
        return min;
    }

    /**
     * Retourne la moyenne des coordonnées Y du Model3D.
     * 
     * @return La moyenne des Y du Model3D.
     */
    public double getCenterY() {
        double res = 0.0;
        for (Point p : this.points) {
            res += p.getY();
        }
        return res / (double) this.points.size();
    }

    /**
     * Retourne la moyenne des coordonnées Z du Model3D.
     * 
     * @return La moyenne des Z du Model3D.
     */
    public double getCenterZ() {
        double res = 0.0;
        for (Point p : this.points) {
            res += p.getZ();
        }
        return res / (double) this.points.size();
    }

    // render methode
    /**
     * Méthode permettant de set la couleur des faces en fonction de la lumiere
     */
    public void setGrayScale() {
        for (int i = 0; i < faces.length; i++) {
            this.faces[i].setGray();
        }
    }

    /**
     * Méthode calculant la tranche en fonction d'une profondeur
     * 
     * @param profondeur
     * @return liste des points representant la tranche (en pair)
     */
    public List<Point> getCoupe(double profondeur) {
        double tmp;
        List<Point> res = new ArrayList<>();
        List<Point> tmplist = new ArrayList<>();
        for (Face face : faces) {
            tmplist.clear();
            for (int i = 0; i < face.getPointslength(); i++) {
                tmp = face.getTheta(i, profondeur);
                if (tmp <= 1 && tmp >= 0) {
                    tmplist.add(new Point(
                            (tmp * face.getPointX(i)) + ((1 - tmp) * face.getPointX((i + 1) % face.getPointslength())),
                            (tmp * face.getPointY(i)) + ((1 - tmp) * face.getPointY((i + 1) % face.getPointslength())),
                            (tmp * face.getPointZ(i))
                                    + ((1 - tmp) * face.getPointZ((i + 1) % face.getPointslength()))));
                }
            }
            if (tmplist.size() == PAIR)
                res.addAll(tmplist);
        }

        return res;
    }

    // DELEGATE METHODE
    /**
     * Permet d'ajouter un point à une face données.
     * 
     * @param o
     * @param idx
     */
    public void addPoint(Point o, int idx) {
        this.faces[idx].addPoint(o);
        if (!this.points.contains(o)) {
            this.points.add(o);
        }
    }

    /**
     * Méthode permettant de assigner la couleur d'une face
     * 
     * @param id
     * @param color
     */
    public void setFaceColor(int id, int[] color) {
        this.faces[id].setColor(color[0], color[1], color[2]);
    }

    /**
     * Retourne la liste des points du Model3D.
     * 
     * @return La liste des points du Model3D.
     */
    public List<Point> getPoints() {
        return points;
    }

}