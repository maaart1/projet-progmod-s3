package model.model3D;

public class TooManyPointException extends Exception{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public TooManyPointException() {
        super("zebi il y trop de points");
    }

}
