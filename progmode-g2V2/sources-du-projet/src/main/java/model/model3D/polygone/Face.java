package model.model3D.polygone;

import vecteur.CalculsVectorielsUtils;
import vecteur.Vecteur;

/**
 * Classe "Faces" : constitue une face en fonction de points.
 * 
 * @author Léonard Corre
 *
 */
public class Face implements Comparable<Face> {
	// ATTRIBUTES
	private Point[] p;
	private int idx = 0;
	private int red = 0;
	private int green = 0;
	private int blue = 0;
	private boolean withColor = false;
	public double grayscale;
	public Vecteur normal;

	/**
	 * Constructeur de Faces, en fonction d'un tableau de points.
	 * 
	 * @param p
	 */
	public Face(Point[] p) {
		super();
		this.p = p;
	}

	/**
	 * Constructeur de Face, en fonction d'une taille de tableau.
	 * 
	 * @param t
	 */
	public Face(int t) {
		super();
		this.p = new Point[t];
	}

	/**
	 * Constructeur de Face, en fonction de 3 points.
	 * 
	 * @param x
	 * @param y
	 * @param z
	 */
	public Face(Point x, Point y, Point z) {
		this.p = new Point[3];
		this.p[0] = x;
		this.p[1] = y;
		this.p[2] = z;

	}

	/**
	 * Methode permettant de calculer si un segment coupe un plan
	 * 
	 * 
	 * @param segment
	 * @param profondeur
	 * @return theta
	 */
	public double getTheta(int segment, double profondeur) {
		return (profondeur - (this.p[(segment + 1) % this.idx].getZ()))
				/ (this.p[segment % this.idx].getZ() - this.p[(segment + 1) % this.idx].getZ());
	}

	/**
	 * Permet d'éditer la lumière de la face en question.
	 */
	public void setGray() {
		double res = CalculsVectorielsUtils.getGrayScale(this);
		if (res <= 0) {
			this.grayscale = 0;
		} else {
			this.grayscale = res;
		}
	}

	/**
	 * Affichage de la face sous forme de coordonnées de points (x, y, z).
	 */
	@Override
	public String toString() {
		String res = "";
		for (int i = 0; i < p.length; i++) {
			res += this.p[i] + "" + i + "\n";
		}
		return res;
	}

	/**
	 * Retourne le point à l'indice i.
	 * 
	 * @param i
	 * @return Un point correspondant au point à l'indice i du tableau.
	 */
	public Point getPoint(int i) {
		return this.p[i];
	}

	public void setColor(int red, int green, int blue) {
		this.setRed(red);
		this.setGreen(green);
		this.setBlue(blue);
		this.setWithColor(true);
	}

	/**
	 * Retourne le tableau de points.
	 * 
	 * @return Un point correspondant au tableau de points.
	 */
	public Point[] getPoints() {
		return this.p;
	}

	/**
	 * Retourne la moyenne des points de coordonnées Z de la face.
	 * 
	 * @return Un double correspondant à la moyenne des coordonnées Z des points de
	 *         la faces.
	 */
	public Double getAverageZ() {
		Double avg = 0.0;
		for (Point points : this.p) {
			avg += points.getZ();
		}
		return avg / this.p.length;
	}

	/**
	 * Ajoute un point au tableau de point.
	 * 
	 * @param o
	 */
	public void addPoint(Point o) {
		this.p[this.idx] = o;
		this.idx++;
		if (this.p.length == this.idx) {
			this.setGray();
		}
	}

	/**
	 * Retoune la valeur rgb de la couleur rouge.
	 * 
	 * @return Un int correspondant à la valeur rouge de la face.
	 */
	public int getRed() {
		return red;
	}

	/**
	 * Change la valeur de la couleur rouge de la face.
	 * 
	 * @param red
	 */
	public void setRed(int red) {
		this.red = red;
	}

	/**
	 * Retoune la valeur rgb de la couleur verte.
	 * 
	 * @return Un int correspondant à la valeur verte de la face.
	 */
	public int getGreen() {
		return green;
	}

	/**
	 * Change la valeur de la couleur verte de la face.
	 * 
	 * @param green
	 */
	public void setGreen(int green) {
		this.green = green;
	}

	/**
	 * Retoune la valeur rgb de la couleur bleu.
	 * 
	 * @return Un int correspondant à la valeur bleu de la face.
	 */
	public int getBlue() {
		return blue;
	}

	/**
	 * Change la valeur de la couleur bleu de la face.
	 * 
	 * @param blue
	 */
	public void setBlue(int blue) {
		this.blue = blue;
	}

	/**
	 * Permet de vérifier si la face est en couleur.
	 * 
	 * @return True ou false si la face est en couleur.
	 */
	public boolean isWithColor() {
		return withColor;
	}

	/**
	 * Change la valeur du boolean withColor.
	 * 
	 * @param withColor
	 */
	public void setWithColor(boolean withColor) {
		this.withColor = withColor;
	}

	// DELEGATED METHODE
	public double getPointX(int id) {
		return this.p[id].getX();
	}

	public double getPointY(int id) {
		return this.p[id].getY();
	}

	public double getPointZ(int id) {
		return this.p[id].getZ();
	}

	public int getPointslength() {
		return this.p.length;
	}

	public double getNormalZ() {
		return this.normal.getZ();
	}

	@Override
	public int compareTo(Face o) {
		return this.getAverageZ().compareTo(o.getAverageZ());
	}
}
