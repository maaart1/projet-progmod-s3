package model.model3D.polygone;
/**
 * Classe "Point" : constinue un point, permettant de constituer des faces.
 * 
 * @author Léonard Corre
 *
 */
public class Point {
		/**
	 * Affichage des points sous la forme : x, y, z.
	 * 
	 * @return Un string correspondant au coordonnées du point.
	 */
	@Override
	public String toString() {
		return "Point [x=" + x + ", y=" + y + ", z=" + z + "]";
	}

	protected double x;
	protected double y;
	protected double z;
	/**
	 * Constructeur de Points.
	 * 
	 * @param x
	 * @param y
	 * @param z
	 */
	public Point(double x, double y, double z) {
		super();
		this.x = x;
		this.y = y;
		this.z = z;
	}


	/**
	 * Retourne les coordonnées de X.
	 * 
	 * @return Un double correspondant à la coordonnées de X.
	 */
	public double getX() {
		return x;
	}
	/**
	 * Change la coordonnées de X.
	 * 
	 * @param x
	 */
	public void setX(double x) {
		this.x = x;
	}
	/**
	 * Retourne les coordonnées de Y.
	 * 
	 * @return Un double correspondant à la coordonnées de Y.
	 */
	public double getY() {
		return y;
	}
	/**
	 * Change la coordonnées de Y.
	 * 
	 * @param y
	 */
	public void setY(double y) {
		this.y = y;
	}
	/**
	 * Retourne les coordonnées de Z.
	 * 
	 * @return Un double correspondant à la coordonnées de Z.
	 */
	public double getZ() {
		return z;
	}
	/**
	 * Change la coordonnées de Z.
	 * 
	 * @param z
	 */
	public void setZ(double z) {
		this.z = z;
	}
	/**
	 * Retourne les coordonnées de X et Y sous forme de tableau.
	 * 
	 * @return Un tableau de double correspondant au coordonnées de X et Y.
	 */
	public Double[] getXYCoordinate() {
		return new Double[] { this.x, this.y };
	}

	/**
	 * Permet de faire la rotation du point.
	 * 
	 * @param rx
	 * @param ry
	 * @param rz
	 */
	public void rotate(double rx, double ry, double rz) {
		this.rotateX(rx);
		this.rotateY(ry);
		this.rotateZ(rz);
	}
	/**
	 * Permet de faire la rotation de la coordonnées X du points.
	 * 
	 * @param rx
	 */
	private void rotateX(double rx) {
		double tmpY;
		double tmpZ;
		tmpY = this.y;
		tmpZ = this.z;
		this.y = (Math.cos(rx) * tmpY) + (-Math.sin(rx) * tmpZ);
		this.z = (Math.sin(rx) * tmpY) + (Math.cos(rx) * tmpZ);
	}
	/**
	 * Permet de faire la rotation de la coordonnées Y du points.
	 * 
	 * @param ry
	 */
	private void rotateY(double ry) {
		double tmpX;
		double tmpZ;

		tmpX = this.x;
		tmpZ = this.z;
		this.x = (Math.cos(ry) * tmpX) - (Math.sin(ry) * tmpZ);
		this.z = (Math.sin(ry) * tmpX) + (Math.cos(ry) * tmpZ);
	}
	/**
	 * Permet de faire la rotation de la coordonnées Z du points.
	 * 
	 * @param rz
	 */
	private void rotateZ(double rz) {
		double tmpX;
		double tmpY;

		tmpX = this.x;
		tmpY = this.y;
		this.x = (Math.cos(rz) * tmpX) + (Math.sin(rz) * tmpY);
		this.y = (-Math.sin(rz) * tmpX) + (Math.cos(rz) * tmpY);
	}
	/**
	 * Permet de faire la translation du point.
	 * 
	 * @param tx
	 * @param ty
	 * @param tz
	 */
	public void translation(double tx, double ty, double tz) {
		this.x += tx;
		this.y += ty;
		this.z += tz;
	}
	/**
	 * Permet de faire le zoom du points.
	 * 
	 * @param nui
	 */
	public void zoom(double nui) {
		this.x = this.x * nui;
		this.y = this.y * nui;
		this.z = this.z * nui;
	}
	/**
	 * Méthode equals.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		return false;
	}

}
