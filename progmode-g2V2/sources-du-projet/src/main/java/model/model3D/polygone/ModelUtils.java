package model.model3D.polygone;

import model.model3D.Model3D;

public class ModelUtils {
    private ModelUtils (){

    }
	/**
	 * Permet de faire la translation du Model3D avec le offSet.
	 * 
	 * @param tx
	 * @param ty
	 */
    public static void translation(Model3D data,double tx, double ty) {
        data.xOffset += tx;
        data.yOffset += ty;
        ModelUtils._translation(data,new double[]{tx,ty,0});
        data.notifyObserver();

    }

        	/**
	 * Permet de faire la translation du Model3D sans le offSet.
	 * 
     * @param data model a translater
	 * @param translaData coordonee de translation en x et y
	 */
    private static void _translation(Model3D data,double[] translaData) {
        for (Point p : data.points) {
            p.translation(translaData[0], translaData[1], translaData[2]);
        }

    }

	/**
	 * Permet de faire le zoom du Model3D.
	 * @param data model a zoomer
	 * @param nui ratio de zoom
	 */
    public static void zoom(Model3D data,double nui) {
        ModelUtils._translation(data,new double[]{-data.xOffset, -data.yOffset, 0});
        for (Point p : data.points) {
            p.zoom(nui);
        }
        ModelUtils._translation(data,new double[]{data.xOffset, data.yOffset, 0});
        data.notifyObserver();
    }

	/**
	 * Permet de faire la rotation du Model3D.
	 * 
	 * @param rotaData tableau de coordonee de rotation
	 */
    public static void rotate(Model3D data, double[] rotaData) {
        ModelUtils._translation(data, new double[]{-data.xOffset, -data.yOffset, 0});
        for (Point p : data.points) {
            p.rotate(rotaData[0], rotaData[1], rotaData[2]);
        }
        ModelUtils._translation(data,new double []{data.xOffset, data.yOffset, 0});
        data.setGrayScale();
        data.sortByAverageZ();
        data.notifyObserver();
    }


	/**
	 * Place le Model3D au centre du Canva.
	 */
    public static void center(Model3D data) {
     ModelUtils._translation(data,new double[]{-data.getCenterX(), -data.getCenterY(), -data.getCenterZ()});
     data.xOffset = 0;
     data.yOffset = 0;
    }

    public static void frame(Model3D data,double length, double height) {
        double scl = length / (data.getMaxX() - data.getMinX());
        double sch = height / (data.getMaxY() - data.getMinY());
        ModelUtils.zoom(data,((sch < scl) ? sch : scl));
    }


}
