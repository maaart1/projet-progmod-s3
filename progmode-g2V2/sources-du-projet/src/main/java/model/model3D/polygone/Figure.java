package model.model3D.polygone;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;
import model.model3D.Model3D;
import view.interface_fxml.MainController;
/**
 * Classe "Figure" : constitue une Figure contenant les informations du fichiers ply.
 * 
 * @author Martin Thibaut, Léonard Corre, Hugo Dutoit
 *
 */
public class Figure {
	protected String name;
	protected String path;
	protected int nbFaces;
	protected int nbPoints;
	protected String author;
	protected Timeline timeLine;
	protected Model3D data;
	protected MainController mc;
	/**
	 * Constructeur de Figure.
	 */
	public Figure() {
		this.nbFaces = 0;
		this.nbPoints = 0;
		this.name = "";
		this.path = "";
		this.author = "inconnu";
	}
	/**
	 * Constructeur de Figure.
	 * @param info
	 * @param listsSize
	 * @param data
	 * @param mainController
	 */
	public Figure(String[] info, int[] listsSize, Model3D data, MainController mainController) {
		this.name = info[0];
		this.path = info[1];
		this.nbFaces = listsSize[0];
		this.nbPoints = listsSize[1];
		this.data = data;
		this.mc = mainController;
		this.author = info[2];
		this.timeLine = new Timeline(new KeyFrame(
				Duration.millis((MainController.TMPSREVO * 1000) / MainController.NBFRAMEPARREVO), this.mc::doStep));
		this.timeLine.setCycleCount(Timeline.INDEFINITE);
	}
	/**
	 * Permet de démarrer la rotation continue de la Figure.
	 */
	public void startRotation() {
		this.timeLine.play();
	}
	/**
	 * Permet d'arréter la rotation continue de le Figure.
	 */
	public void stopRotation() {
		this.timeLine.stop();
	}
	/**
	 * Retourne le nombre de faces de la Figure.
	 * @return
	 */
	public int getNbFaces() {
		return nbFaces;
	}
	/**
	 * Méthode permettant d'obtenir l'auteur de la Figure.
	 * @return Un string correspondant à l'auteur de la Figure.
	 */
	public void setNbFaces(int nbFaces) {
		this.nbFaces = nbFaces;
	}

	public int getNbPoints() {
		return nbPoints;
	}

	public void setNbPoints(int nbPoints) {
		this.nbPoints = nbPoints;
	}

	public String getName() {
		return name;
	}

	public void setName(String nameOfFile) {
		this.name = nameOfFile;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String pathOfFile) {
		this.path = pathOfFile;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
	/**
	 * Permet d'obtenir le Model3D de la Figure.
	 * @return Un Model3D correspondant au Model3D de la Figure.
	 */
	public Model3D getData() {
		return data;
	}

	public void setData(Model3D data) {
		this.data = data;
	}

}
