package exeption;

public class ColorReadException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ColorReadException() {
		super("Erreur dans la lecture des couleurs");
	}

}
