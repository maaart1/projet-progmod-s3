package exeption;
/**
 * Classe "FacesArrayIsFullException"
 * 
 * @author Hugo Dutoit
 *
 */
public class FacesArrayIsFullException extends Exception {
	private static final long serialVersionUID = 1L;
    public FacesArrayIsFullException() {
        super("This model faces is full");
    }
}
