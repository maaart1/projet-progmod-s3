package vecteur;


import model.model3D.polygone.Face;

public class CalculsVectorielsUtils {

    private CalculsVectorielsUtils (){

    }
    public static Vecteur SOURCE = new Vecteur(0, 0, 1);
	/**
	 * calcule le vecteur normal a la face passe en paramettre
	 * 
	 * @param face la face dont on veut la norme
	 * @return vecteur normal de la face
	*/
    public static Vecteur getNorme(Face face){
        Vecteur tmp = new Vecteur(face.getPoint(0),face.getPoint(1));
        Vecteur tmp1 =  new Vecteur(face.getPoint(0),face.getPoint(2));
        return Vecteur.normalize(tmp.getProduitVectoriel(tmp1));

    } 
    	/**
	 * Retourne la lumière de la face en parametres.
	 * 
	 * @param face
	 * @return Un double correspondant au produit scalaire du vecteur de la face et
	 *         celui de la lumière.
	 */
    public static double getGrayScale(Face face){
        face.normal=CalculsVectorielsUtils.getNorme(face);
        SOURCE.normalize();
        return Vecteur.produitScalaire(face.normal, SOURCE);
    }
}
