package vecteur;

import model.model3D.polygone.Point;
/**
 * Classe "Vecteur" : correspond à un vecteur.
 * 
 * @author Léonard Corre
 */
public class Vecteur extends Point{

	/**
	 * Contructeur du vecteur en fonction de 2 points.
	 * 
	 * @param p
	 * @param t
	 */
    public Vecteur(Point p, Point t){
        super(p.getX() - t.getX(),p.getY() - t.getY(),p.getZ() - t.getZ());
    }
	/**
	 * Constructeur du vecteur en fonction de 3 coordonnées.
	 * 
	 * @param d
	 * @param e
	 * @param f
	 */
    public Vecteur(double d, double e, double f) {
        super(d,e,f);
    }
	/**
	 * Retourne le produit vectoriel du vecteur avec celui en parametres.
	 * 
	 * @param other
	 * @return Un vecteur, correspondant au produit vectoriel du vecteur et celui en
	 *         parametres.
	 */
    public Vecteur getProduitVectoriel(Vecteur other) {
        return new Vecteur(this.y*other.z - this.z*other.y, this.z*other.x - this.x*other.z, this.x*other.y - this.y*other.x);
    }
    		/**
	 * Retourne le produit vectoriel.
	 * 
	 * @param A
	 * @param B
	 * @return Un vecteur, correspondant au produit vectoriel des 2 parametres.
	 */
    public  static Vecteur produitVectoriel(Vecteur A, Vecteur B) {
        return new Vecteur(A.y*B.z - A.z*B.y, A.z*B.x - A.x*B.z, A.x*B.y - A.y*B.x);
    }
    		/**
	 * Retourne le vecteur normale unitaires.
	 * 
	 * @param A
	 * @param B
	 * @param C
	 * @return Un vecteur correspondant au vecteur normale unitaire.
	 */
    public  static Vecteur getVNU(Vecteur A, Vecteur B, Vecteur C) {
        return Vecteur.normalize(Vecteur.produitVectoriel(Vecteur.substract(B, A), Vecteur.substract(C, A)));
    }

    	/**
	 * Retourne la soustraction du vecteur avec celui en parametres.
	 * 
	 * @param other
	 * @return Un vecteur correspondant à la soustraction du vecteur avec celui en
	 *         parametres.
	 */
    public Vecteur substract(Vecteur other){
		return new Vecteur(this.x - other.x, this.y - other.y, this.z - other.z);
    }
    
	/**
	 * Retourne la soustraction des vecteurs.
	 * 
	 * @param first
	 * @param other
	 * @return Un vecteur correspondant à la soustraction des vecteurs.
	 */
    public static Vecteur substract(Vecteur first, Vecteur other){
		return new Vecteur(first.x - other.x, first.y - other.y, first.z - other.z);
    }

    public void normalize() {
        double t = x*x + y*y + z*z;
        if (t != 0 && t != 1) t = (double) (1 / Math.sqrt(t));
        x *= t;
        y *= t;
        z *= t;
    }

    public double  getNorme() {
        double t = x*x + y*y + z*z;
        return Math.sqrt(t);
    }
  	/**
	 * Retourne le produit scalaire des vecteurs.
	 * 
	 * @param A
	 * @param B
	 * @return Un double correspondant au produit scalaire des vecteurs en
	 *         parametres.
	 */
    public final static Double produitScalaire(Vecteur A, Vecteur B) {
        return  A.x * B.x + A.y * B.y + A.z * B.z;
        
    }
        	/**
	 * Retourne le vecteur en vecteur normale.
	 * 
	 * @param A
	 * @return Un vecteur correspondant au vecteur normale du vecteur en parametres.
	 */
    public final static Vecteur normalize(Vecteur A) {
        double t = A.x*A.x + A.y*A.y + A.z*A.z;
        if (t != 0 && t != 1) t = (double)(1 / Math.sqrt(t));
        return new Vecteur(A.x*t, A.y*t, A.z*t);
    }

    @Override  
    public boolean equals(Object obj) {  
        if (obj == null) { return false; }  
            if(this.x != ((Vecteur) obj).x && this.y != ((Vecteur) obj).y && this.z != ((Vecteur) obj).z  ){
                return false;
            }
        return true;
    }
 

    


}