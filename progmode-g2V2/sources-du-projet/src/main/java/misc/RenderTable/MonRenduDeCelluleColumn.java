package misc.RenderTable;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.TableCell;
import model.model3D.polygone.Figure;
	/**
	 * Classe Interne "MonRenduDeCelluleStringColumn" : permet d'afficher la colonne
	 * Name de la table view avec le fond noir ainsi que le nom du fichiern nombres
	 * de faceset nombres de points.
	 * 
	 * @author Martin Thibaut
	 */
class MonRenduDeCelluleColumn<K> extends TableCell<Figure, K> {

	@Override
	public void updateItem(K item, boolean b) {
		super.updateItem(item, b);
		if (item != null && !b) {

			Canvas c = new Canvas(500, 20);
			GraphicsContext gc = c.getGraphicsContext2D();
			gc.fillText(item.toString(), 0, 16);
			setStyle("-fx-background-color: #313131;");
			setGraphic(c);
		} else {
			setGraphic(null);
			setStyle("-fx-background-color: #313131;");
		}
	}
}