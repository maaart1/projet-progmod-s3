package misc.RenderTable;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import model.model3D.polygone.Figure;

/**
 * Classe TableCellFactory rend jolie les cellules du tableau
 * 
 * @author Hugo Dutoit
 */
public class TableCellFactory<K> implements Callback<TableColumn<Figure, K>, TableCell<Figure, K>> {

    @Override
    public TableCell<Figure, K> call(TableColumn<Figure, K> param) {
        return new MonRenduDeCelluleColumn();
    }

}
