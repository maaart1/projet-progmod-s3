package misc.MOS;

import java.util.ArrayList;
import java.util.List;
/**
 * Classe Abstract "Subject" : MVC.
 * 
 * @author Hugo Dutoit
 *
 */
public abstract class AbstractSubject {
    //ATTRIBUTE
    private List<Observer> Observers = new ArrayList<>();


    //ATTACH DETACH
    public void attach(Observer o)  {
        if (this.Observers.contains(o))
            return;
        this.Observers.add(o);
    }

    public void detach(Observer o) throws NoSuchObserverException {
        if  (!this.Observers.contains(o))
            throw new NoSuchObserverException();
    }

    //NOTIFY function
    public void notifyObserver(Object data){
        for(Observer o:this.Observers){
            o.update(data);
        }
    }

    public void notifyObserver(){
        for(Observer o: this.Observers){
            o.update();
        }
    }


    public class NoSuchObserverException extends Exception{
        /**
         *
         */
        private static final long serialVersionUID = 1L;

        public NoSuchObserverException() {
            super("This Observer isn't attach to this Subject");
        }
    }

}