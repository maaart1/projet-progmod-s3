package misc.MOS;
/**
 * Interface "Observer" : MVC.
 * 
 * @author Hugo Dutoit
 *
 */
public interface Observer 
{
    public void update(Object data);
    public void update();
}
