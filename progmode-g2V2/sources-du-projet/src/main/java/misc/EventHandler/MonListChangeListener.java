package misc.EventHandler;

import javafx.collections.ListChangeListener;
import model.model3D.polygone.Figure;
import model.model3D.polygone.ModelUtils;
import view.interface_fxml.MainController;
import view.interface_fxml.trancheView.TrancheView;

/**
 * Classe MonListChangeListener gere le changement de model sur la tableview
 * 
 * 
 * @author Hugo Dutoit
 */
public class MonListChangeListener implements ListChangeListener<Figure> {
	/**
	 *
	 */
	private final MainController mainController;

	/**
	 * @param mainController
	 */
	public MonListChangeListener(MainController mainController) {
		this.mainController = mainController;
	}

	@Override
	public void onChanged(javafx.collections.ListChangeListener.Change<? extends Figure> c) {
		if (!this.mainController.maTableView.getItems().isEmpty()) {
			this.mainController.resetSliders();
			this.mainController.data = this.mainController.maTableView.getSelectionModel().getSelectedItem().getData();
			this.mainController.maTableView.getSelectionModel().getSelectedItems().get(0).startRotation();
			for (TrancheView v : this.mainController.views) {
				v.setData(this.mainController.data);

			}
			this.mainController.monLabelPoint
					.setText("Nombres de points : " + this.mainController.data.getPointslength());
			this.mainController.monLabelFace.setText("Nombres de faces : " + this.mainController.data.getFacesLength());
			this.mainController.monLabelAuteur.setText(
					"Auteur : " + this.mainController.maTableView.getSelectionModel().getSelectedItem().getAuthor());
			this.mainController.attach();
			ModelUtils.center(this.mainController.data);
			ModelUtils.frame(this.mainController.data, this.mainController.screen.getWidth(),
					this.mainController.screen.getHeight() / 2);
			ModelUtils.translation(this.mainController.data, this.mainController.screen.getWidth() / 2,
					this.mainController.screen.getHeight() / 2);
			this.mainController.setTrancheSlider();
			this.mainController.data.sortByAverageZ();
		}
	}

}