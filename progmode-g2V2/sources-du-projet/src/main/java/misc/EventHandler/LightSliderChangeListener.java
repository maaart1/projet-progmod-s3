package misc.EventHandler;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import vecteur.CalculsVectorielsUtils;
import view.interface_fxml.MainController;

/**
 * Classe LightSliderChangeListener permet d'avoir les grayscales des faces en
 * fonction du slider de la lumière
 * 
 * 
 * @author Hugo Dutoit
 */
public final class LightSliderChangeListener implements ChangeListener<Number> {
	/**
	 *
	 */
	private final MainController mainController;

	/**
	 * @param mainController
	 */
	public LightSliderChangeListener(MainController mainController) {
		this.mainController = mainController;
	}

	@Override
	public void changed(ObservableValue<? extends Number> observable, Number old, Number nui) {
		CalculsVectorielsUtils.SOURCE.rotate(0, (double) nui - (double) old, 0);
		this.mainController.data.setGrayScale();
		this.mainController.data.notifyObserver();
	}
}