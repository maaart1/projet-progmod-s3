package misc.EventHandler;

import javafx.event.EventHandler;
import javafx.scene.input.ScrollEvent;
import model.model3D.polygone.ModelUtils;
import view.interface_fxml.MainController;

/**
 * Classe ZoomScroolHandler gestion du zoom avec la molette de la souris
 * 
 * @author Hugo Dutoit
 */
public class ZoomScrollHanlder implements EventHandler<ScrollEvent> {
	/**
	 *
	 */
	private final MainController mainController;

	/**
	 * @param mainController
	 */
	public ZoomScrollHanlder(MainController mainController) {
		this.mainController = mainController;
	}

	@Override
	public void handle(ScrollEvent e) {
		if (e.getDeltaY() < 0)
			ModelUtils.zoom(this.mainController.data, mainController.SCALEM);
		else
			ModelUtils.zoom(this.mainController.data, MainController.SCALEP);
		this.mainController.setTrancheSlider();
	}

}
