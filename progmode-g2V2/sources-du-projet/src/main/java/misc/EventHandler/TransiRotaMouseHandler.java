package misc.EventHandler;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import model.model3D.polygone.ModelUtils;
import view.interface_fxml.MainController;

/**
 * Classe TransiRotaMouseHandler gestion d'evenement de la souris pour le
 * deplacement
 * 
 * @author Hugo Dutoit
 */
public final class TransiRotaMouseHandler implements EventHandler<MouseEvent> {
	/**
	 *
	 */
	private final MainController mainController;

	/**
	 * @param mainController
	 */
	public TransiRotaMouseHandler(MainController mainController) {
		this.mainController = mainController;
	}

	@Override
	public void handle(MouseEvent e) {
		if (e.isSecondaryButtonDown()) {
			ModelUtils.rotate(this.mainController.data, new double[] { (this.mainController.tmpY - e.getY()) / 100,
					(this.mainController.tmpX - e.getX()) / 100, 0 });
		} else {
			ModelUtils.translation(this.mainController.data, e.getX() - this.mainController.tmpX,
					e.getY() - this.mainController.tmpY);
		}
		this.mainController.tmpX = e.getX();
		this.mainController.tmpY = e.getY();
	}
}