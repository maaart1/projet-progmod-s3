package view.interface_fxml.trancheView;

import java.util.List;

import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Slider;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.Window;
import misc.MOS.Observer;
import model.model3D.Model3D;

import model.model3D.polygone.Point;

public class TrancheView extends Stage implements Observer{
    private Model3D data;

    private Canvas screen;
    private Slider trancheSlider;

    public TrancheView (Model3D data){
        this.setData(data);

        screen = new Canvas(300,250);

        initTrancheSlider();

        VBox root=new VBox(screen,trancheSlider);

        root.setStyle("-fx-background-color: #636e72");
        this.setTitle("Tranche View");
        this.initOwner(Window.getWindows().get(0));
        this.setResizable(false);
        this.setScene(new Scene(root, 300, 300));
        this.show();
        data.notifyObserver();
    }
	private void setTrancheSlider() {
		trancheSlider.setMax(data.getMaxZ());
        trancheSlider.setMin(data.getMinZ());
	}

    private void renderModel(){
        if (data != null) {
			GraphicsContext gc = screen.getGraphicsContext2D();
			gc.clearRect(0, 0, screen.getWidth(), screen.getHeight());
            renderTranche(gc);
		}
    }
    private void initTrancheSlider(){
        trancheSlider = new Slider();
        trancheSlider.setOrientation(Orientation.HORIZONTAL);
        trancheSlider.setPrefWidth(300);
        trancheSlider.setBlockIncrement(10);
        trancheSlider.valueProperty().addListener((obv, old, nui) -> {
		    data.notifyObserver();
        });
    }
    private void renderTranche(GraphicsContext gc) {
		Point tmppoint,tmppoint2;
		List<Point> tmp = data.getCoupe(trancheSlider.getValue());
		for (int i = 0; i<tmp.size();i+=2) {
			tmppoint = tmp.get(i);
			tmppoint2 = tmp.get(i+1);
			gc.strokeLine(tmppoint.getX()/3,tmppoint.getY()/3 ,tmppoint2.getX()/3, tmppoint2.getY()/3);
		}
    }
    
    public void setData(Model3D data){
        this.data = data;
        this.data.attach(this);
    }

    @Override
    public void update(Object data) {
        // TODO Auto-generated method stub
        this.renderModel();
    }

    @Override
    public void update() {
        // TODO Auto-generated method stub
        this.renderModel();
        setTrancheSlider();
    }
}
