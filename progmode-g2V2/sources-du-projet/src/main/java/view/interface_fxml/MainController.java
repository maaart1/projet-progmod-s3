package view.interface_fxml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import exeption.ColorReadException;
import exeption.FacesArrayIsFullException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import misc.MOS.Observer;
import misc.RenderTable.TableCellFactory;
import model.lecture_ply.NotPlyFileException;
import model.lecture_ply.Read;
import model.model3D.Model3D;
import model.model3D.ModelNull;
import model.model3D.polygone.Face;
import model.model3D.polygone.Figure;
import model.model3D.polygone.ModelUtils;
import model.model3D.polygone.Point;
import view.interface_fxml.trancheView.TrancheView;
import misc.EventHandler.MonListChangeListener;
import misc.EventHandler.ZoomScrollHanlder;
import misc.EventHandler.TransiRotaMouseHandler;
import misc.EventHandler.LightSliderChangeListener;

/**
 * Classe "MonController" : permet de contrôler toutes les fonctionnalités du
 * logiciel.
 * 
 * @author Martin Thibaut, Léonard Corre, Hugo Dutoit
 * @implNote Implémente l'interfaçe Observer.
 */
public class MainController implements Observer {
	/**
	 *
	 */
	@FXML
	public TableView<Figure> maTableView;
	@FXML
	TableColumn<Figure, String> nameTblC;
	@FXML
	TableColumn<Figure, Integer> nbPointsTblC;
	@FXML
	TableColumn<Figure, Integer> nbFacesTblC;
	@FXML
	private MenuItem monButtonOpen;
	@FXML
	public Canvas screen;
	@FXML
	private BorderPane monBorderPane;
	@FXML
	private ToggleButton monToggleButton;
	@FXML
	private Pane monPane;
	@FXML
	private MenuBar maMenuBar;
	@FXML
	MenuBar monMenuBar;
	@FXML
	private ColorPicker colorPickerBG;
	@FXML
	private ColorPicker colorPickerFG;
	@FXML
	private Slider rotaXSlider;
	@FXML
	private Slider rotaYSlider;
	@FXML
	private Slider rotaZSlider;
	@FXML
	private Button zoomPlus;
	@FXML
	private Button zoomMoins;
	@FXML
	private TextArea maTextArea;
	@FXML
	private Button transMinusX;
	@FXML
	private Button transMinusY;
	@FXML
	private Button transPlusX;
	@FXML
	ToggleButton startRotationX;
	@FXML
	ToggleButton startRotationY;
	@FXML
	ToggleButton startRotationZ;
	@FXML
	Button stopRotation;
	@FXML
	public Label monLabelAuteur;
	@FXML
	public Label monLabelPoint;
	@FXML
	public Label monLabelFace;
	@FXML
	private Button transPlusY;
	@FXML
	private CheckBox trait;
	@FXML
	CheckBox fill;
	@FXML
	CheckBox colorOriginale;
	@FXML
	CheckBox grayscale;
	@FXML
	CheckBox tranchecheck;
	@FXML
	Slider trancheslider;
	@FXML
	Slider lumiereslider;
	@FXML
	Button trancheView;

	/////////////////////////////////////////////

	private FileChooser fileC;
	public List<TrancheView> views = new ArrayList<>();
	private DirectoryChooser directoryC;

	public Model3D data = new ModelNull();
	public Double tmpY;
	public Double tmpX;
	public static final int TMPSREVO = 10;
	public static final int NBFRAMEPARREVO = 100;
	private static final Read READER = new Read();
	public static final double SCALEP = 1.15;
	public static final double SCALEM = 0.85;
	private static final double TRANSI = 50;

	Alert alert = new Alert(AlertType.ERROR, "Error !", ButtonType.CLOSE);
	Stage stage = new Stage();

	/////////////////////////////////////////////
	/**
	 * Méthode qui s'exécuter au lancement du logiciel afin de faire toute les
	 * initialisations.
	 * 
	 * @throws Exception
	 */
	public void initialize() throws Exception {

		// INTERFACE INSTANCE
		fileC = new FileChooser();
		directoryC = new DirectoryChooser();
		// color setting
		colorPickerBG.setValue(Color.GRAY);
		colorPickerFG.setValue(Color.BLACK);

		monBorderPane.setPrefHeight((Screen.getPrimary().getBounds().getHeight() / 3) * 2);
		monBorderPane.setPrefWidth((Screen.getPrimary().getBounds().getWidth()));
		// Slider settings
		this.initRotaSliders();
		// mouse event settings
		this.initMouseEvent();
		// Button Settings
		this.initBtn();

		trancheslider.setBlockIncrement(10);

		lumiereslider.setBlockIncrement(Math.PI / 10);
		lumiereslider.setMax(Math.PI);
		lumiereslider.setMin(-Math.PI);
		lumiereslider.setValue(0);

		trancheView.setOnAction(e -> this.views.add(new TrancheView(data)));

		fill.setOnAction(e -> data.notifyObserver());
		trait.setOnAction(e -> data.notifyObserver());
		colorOriginale.setOnAction(e -> data.notifyObserver());
		grayscale.setOnAction(e -> data.notifyObserver());
		tranchecheck.setOnAction(e -> data.notifyObserver());

		trancheslider.valueProperty().addListener((obv, old, nui) -> data.notifyObserver());

		lumiereslider.valueProperty().addListener(new LightSliderChangeListener(this));

		// EVENT LISTENER INITIALASING
		maTableView.getSelectionModel().getSelectedItems().addListener(new MonListChangeListener(this));

		colorPickerFG.setOnAction(e -> data.notifyObserver());
		colorPickerBG.setOnAction(e -> data.notifyObserver());

		// LIST FACTORY
		this.nameTblC.setCellValueFactory(new PropertyValueFactory<>("name"));
		this.nbPointsTblC.setCellValueFactory(new PropertyValueFactory<>("nbPoints"));
		this.nbFacesTblC.setCellValueFactory(new PropertyValueFactory<>("nbFaces"));

		this.nameTblC.setCellFactory(new TableCellFactory<String>());

		this.nbPointsTblC.setCellFactory(new TableCellFactory<Integer>());

		this.nbFacesTblC.setCellFactory(new TableCellFactory<Integer>());
	}

	/**
	 * Méthode d'initialisation de tout les buttons du logiciel.
	 */
	private void initBtn() {
		this.initTransiBtn();
		this.initZoomBtn();
	}

	/**
	 * Méthode d'initialisation des buttons zoom du logiciel.
	 */
	private void initZoomBtn() {
		zoomPlus.setOnAction(e -> {
			ModelUtils.zoom(data, SCALEP);
			setTrancheSlider();
		});
		zoomMoins.setOnAction(e -> {
			ModelUtils.zoom(data, -SCALEP);
			setTrancheSlider();
		});
	}

	/**
	 * Méthode d'initialisation des buttons translation du logiciel
	 * 
	 */
	private void initTransiBtn() {
		transMinusX.setOnAction(e -> ModelUtils.translation(data, -TRANSI, 0));
		transMinusY.setOnAction(e -> ModelUtils.translation(data, 0, TRANSI));
		transPlusX.setOnAction(e -> ModelUtils.translation(data, TRANSI, 0));
		transPlusY.setOnAction(e -> ModelUtils.translation(data, 0, -TRANSI));
	}

	/**
	 * Méthode d'initialisation de tout les MouseEvent du logiciel.
	 */
	private void initMouseEvent() {
		this.initMouseTransi();
		monBorderPane.setOnScroll(new ZoomScrollHanlder(this));
	}

	/**
	 * Méthode d'initialisation des MouseEvent translation du logiciel.
	 */
	private void initMouseTransi() {
		monBorderPane.setOnMousePressed(e -> {
			tmpX = e.getX();
			tmpY = e.getY();
		});
		monBorderPane.setOnMouseDragged(new TransiRotaMouseHandler(this));
	}

	/**
	 * Méthode d'initialisation de tout les Sliders du logiciel.
	 */
	private void initRotaSliders() {
		// rota Slider Settings
		// rotaX
		MainController.initRotaSlider(rotaXSlider);
		rotaXSlider.valueProperty().addListener(
				(obv, old, nui) -> ModelUtils.rotate(data, new double[] { (double) nui - (double) old, 0, 0 }));
		// rotaY
		MainController.initRotaSlider(rotaYSlider);
		rotaYSlider.valueProperty().addListener(
				(obv, old, nui) -> ModelUtils.rotate(data, new double[] { 0, (double) nui - (double) old, 0 }));
		// rotaZ
		MainController.initRotaSlider(rotaZSlider);
		rotaZSlider.valueProperty().addListener(
				(obv, old, nui) -> ModelUtils.rotate(data, new double[] { 0, 0, (double) nui - (double) old }));
	}

	/**
	 * Méthode d'initialisation des Sliders rotation du logiciel.
	 */
	private static void initRotaSlider(Slider sl) {
		sl.setMax(Math.PI);
		sl.setMin(-Math.PI);
		sl.setBlockIncrement(Math.PI / 100);
		sl.setValue(0);
	}

	/**
	 * Méthode permettant le fonctionnement de la rotation continue.
	 * 
	 * @param event
	 */
	@SuppressWarnings("exports")
	public void doStep(ActionEvent event) {
		if (this.startRotationX.isSelected())
			ModelUtils.rotate(data, new double[] { (Math.PI * 2) / NBFRAMEPARREVO, 0, 0 });
		if (this.startRotationY.isSelected())
			ModelUtils.rotate(data, new double[] { 0, (Math.PI * 2) / NBFRAMEPARREVO, 0 });
		if (this.startRotationZ.isSelected())
			ModelUtils.rotate(data, new double[] { 0, 0, (Math.PI * 2) / NBFRAMEPARREVO });
	}

	/**
	 * Méthode permettant de reset les slider a 0.
	 */
	public void resetSliders() {
		rotaXSlider.setValue(0);
		rotaYSlider.setValue(0);
		rotaZSlider.setValue(0);
		trancheslider.setValue(0);
	}

	/**
	 * Méthode permettant d'afficher les Model3D.
	 */
	public void renderModel() {
		if (data != null) {
			GraphicsContext gc = screen.getGraphicsContext2D();
			gc.clearRect(0, 0, screen.getWidth(), screen.getHeight());
			if (tranchecheck.isSelected())
				renderTranche(gc);
			else
				renderNormal(gc);
		}
	}

	/**
	 * Méthode permettant d'afficher le model3D en tranche
	 * 
	 * @param gc
	 */
	private void renderTranche(GraphicsContext gc) {
		Point tmppoint,tmppoint2;
		List<Point> tmp = data.getCoupe(trancheslider.getValue());
		for (int i = 0; i<tmp.size();i+=2) {
			tmppoint = tmp.get(i);
			tmppoint2 = tmp.get(i+1);
			gc.strokeLine(tmppoint.getX(),tmppoint.getY() ,tmppoint2.getX(), tmppoint2.getY());
		}
	}

	/**
	 * Méthode permettant d'afficher le model3D dans son entiereté
	 * 
	 * @param gc
	 */
	private void renderNormal(GraphicsContext gc) {
		double[] xCoordFace, yCoordFace;
		int faceLength;
		Point tmpPoint;
		for (Face faces : this.data.getFaces()) {
			faceLength = faces.getPointslength();
			xCoordFace = new double[faceLength];
			yCoordFace = new double[faceLength];
			for (int p = 0; p < faceLength; p++) {
				tmpPoint = faces.getPoint(p);
				xCoordFace[p] = tmpPoint.getX();
				yCoordFace[p] = tmpPoint.getY();
			}
			gc.setLineWidth(1.0);
			double a = 1;
			int r, g, b;
			if (colorOriginale.isSelected() && faces.isWithColor()) {
				r = faces.getRed();
				g = faces.getGreen();
				b = faces.getBlue();
			} else {
				Color tmp = colorPickerBG.getValue();
				r = (int) (tmp.getRed() * 255);
				b = (int) (tmp.getBlue() * 255);
				g = (int) (tmp.getGreen() * 255);
				a = tmp.getOpacity();
			}
			if (grayscale.isSelected()) {
				r *= faces.grayscale;
				g *= faces.grayscale;
				b *= faces.grayscale;
			}
			gc.setFill(Color.rgb(r, g, b, a));
			gc.setStroke(colorPickerFG.getValue());
			if (fill.isSelected() && faces.getNormalZ() > 0)
				gc.fillPolygon(xCoordFace, yCoordFace, faceLength);
			if (trait.isSelected())
				gc.strokePolygon(xCoordFace, yCoordFace, faceLength);
		}
	}

	/**
	 * Méthode permettant d'ouvrir un file depuis un menu
	 */
	public void openFile() {
		File file = fileC.showOpenDialog(stage);
		_openFile(file);
	}

	/**
	 * Méthode permettant d'ajouter les fichiers d'un dossier dans la liste des
	 * Model3D du logiciel.
	 */
	public void openFolder() {
		File file = directoryC.showDialog(stage);
		if (file != null) {
			for (int i = 0; i < file.listFiles().length; i++) {
				_openFile(file.listFiles()[i]);
			}
		}
	}

	/**
	 * Méthode permettant d'ouvrir un file et de le placer dans la tableview
	 * 
	 * @param file
	 */
	public void _openFile(File file) {
		if (file != null && !file.isDirectory()) {
			Model3D m;
			try {
				m = READER.ui(file);
				Figure figure = new Figure(
						new String[] { file.getName(), file.getAbsoluteFile().toString(), READER.getAuteur(file) },
						new int[] { m.getFaces().length, m.getPointslength() }, m, this);
				this.maTableView.getItems().add(figure);
			} catch (NumberFormatException | IOException | NotPlyFileException | FacesArrayIsFullException
					| ColorReadException e) {
				alert.setContentText(e.getMessage());
				alert.showAndWait();
			}
		}
	}

	/**
	 * méthode permettant de set le min et max des slider de la vue en tranche
	 */
	public void setTrancheSlider() {
		trancheslider.setMax(data.getMaxZ());
		trancheslider.setMin(data.getMinZ());
	}

	/**
	 * Méthode permattant d'attacher le model à la vue.
	 * 
	 * @throws ObserverAlreadyExist
	 */
	public void attach() {
		data.attach(this);
	}

	/**
	 * Méthode permattant de mettre à jour la vue (MVC), méthode appelée à chaque
	 * notify() du model.
	 * 
	 * @param data
	 */
	@Override
	public void update(Object data) {
		// useless
	}

	/**
	 * Méthode permattant de mettre à jour la vue (MVC), méthode appelée à chaque
	 * notify() du model.
	 * 
	 */
	@Override
	public void update() {
		this.renderModel();
	}
}
