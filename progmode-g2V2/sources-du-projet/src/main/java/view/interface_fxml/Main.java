package view.interface_fxml;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Classe "Main" : permet de lancer le logiciel
 * 
 * @author Martin Thibaut, Léonard Corre, Hugo Dutoit
 */
public class Main extends Application {
		/**
	 * Méthode qui hérite d'Application et qui charge le fichier FXML afin
	 * d'afficher l'interfaçe
	 * 
	 * @param stage
	 * @return void
	 * @throws Exception
	 */
	@Override
	public void start(Stage stage) throws Exception {

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("Main.fxml"));
		Parent root = loader.load();

		Scene scene = new Scene(root, 1366, 768);
		stage.setScene(scene);
		stage.setTitle("3D Generation Application");
		stage.show();
	}
	/**
	 * Méthode main : lance le logiciel
	 * 
	 * @param args
	 * @return void
	 */
	public static void main(String[] args) {
		Application.launch();
	}
}
