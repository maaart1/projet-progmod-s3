package model.lecture_ply;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

import org.junit.jupiter.api.Test;

public class TestsFichierPly {
	Read r = new Read();
	String cubeOk = "ply\r\n" + "format ascii 1.0\r\n" + "comment made by Greg Turk\r\n"
			+ "comment this file is a cube\r\n" + "element vertex 8\r\n" + "property float x\r\n"
			+ "property float y\r\n" + "property float z\r\n" + "element face 6\r\n"
			+ "property list uchar int vertex_index\r\n" + "end_header";

	String cubePasOk = "format ascii 1.0\r\n" + "comment made by Greg Turk\r\n" + "comment this file is a cube\r\n"
			+ "element vertex 8\r\n" + "property float x\r\n" + "property float y\r\n" + "property float z\r\n"
			+ "element face 6\r\n" + "property list uchar int vertex_index";

	String sphereOk = "ply\r\n" + "format ascii 1.0\r\n" + "element vertex 422\r\n" + "property float x\r\n"
			+ "property float y\r\n" + "property float z\r\n" + "element face 840\r\n"
			+ "property list uchar int vertex_indices\r\n" + "end_header";

	@Test
	public void testFichierPlyOk() throws IOException, NotPlyFileException {
		assertTrue(r.isHeaderOk(new BufferedReader(new StringReader(cubeOk))));
		assertEquals(r.getNbPoints(), 8);
		assertEquals(r.getNbFaces(), 6);

		assertTrue(r.isHeaderOk(new BufferedReader(new StringReader(sphereOk))));
		assertEquals(r.getNbPoints(), 422);
		assertEquals(r.getNbFaces(), 840);
	}

	@Test
	public void testFichierPlyCasse() {
		boolean test = false;
		try {
			r.isHeaderOk(new BufferedReader(new StringReader(cubePasOk)));
		} catch (IOException | NotPlyFileException e) {
			test = true;
			e.printStackTrace();
		}
		assertTrue(test);
		assertEquals(r.getNbPoints(), 0);
		assertEquals(r.getNbFaces(), 0);
	}

}
