package model.vecteur;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import model.model3D.polygone.Point;
import vecteur.Vecteur;

public class TestsVecteur {
   
    @Test
	public void testcreatevecteur() {
        Point p = new Point(1,2,3);
        Point t = new Point(3,2,1);
        Vecteur test = new Vecteur(p, t);
        assertEquals(new Vecteur(-2, 0 , 2), test);
    }
    

    @Test
    public void testProduitscalaire(){
        Vecteur test =  new Vecteur(1,2,3);
        Vecteur test2 =  new Vecteur(1,2,3);
        double res = Vecteur.produitScalaire(test,test2);
        assertEquals(res,14.0);

    }

    @Test
    public void testnorme(){
        Vecteur test =  new Vecteur(1,2,3);
        assertEquals(test.getNorme(),Math.sqrt(14.0));

    }

    @Test
    public void testnormaliser(){
        Vecteur test =  new Vecteur(1,2,3);
        test.normalize();
        assertEquals(test,new Vecteur(1/Math.sqrt(14.0),2/Math.sqrt(14.0),3/Math.sqrt(14.0)));
    }


    @Test
    public void testsubstract(){
        Vecteur test =  new Vecteur(1,2,3);
        Vecteur test2 =  new Vecteur(1,2,3);
        assertEquals(Vecteur.substract(test, test2),new Vecteur(0,0,0));
    }

    @Test
    public void testproduitvectoriel(){
        Vecteur test =  new Vecteur(1,2,3);
        Vecteur test2 =  new Vecteur(1,2,3);
        assertEquals(Vecteur.produitVectoriel(test,test2),new Vecteur(0,0,0));
    }

    @Test
    public void testVNU(){
        Vecteur test =  new Vecteur(1,1,1);
        Vecteur test2 =  new Vecteur(2,2,2);
        Vecteur test3 =  new Vecteur(3,3,3);
        assertEquals(Vecteur.getVNU(test,test2,test3),new Vecteur(0,0,0));
    }


}
