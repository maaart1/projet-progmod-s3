package model.model3D;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import exeption.ColorReadException;
import exeption.FacesArrayIsFullException;
import model.lecture_ply.NotPlyFileException;
import model.lecture_ply.Read;

public class TestsModel3D {

    Read reader = new Read();
    Model3D data;
    String model = "ply\nformat ascii 1.0\ncomment made by Greg Turk\ncomment this file is a cube\nelement vertex 8\nproperty float x\nproperty float y\nproperty float z\nelement face 6\nproperty list uchar int vertex_indices\nend_header\n0 0 0\n0 0 100\n0 100 100\n0 100 0\n100 0 0\n100 0 100\n100 100 100\n100 100 0\n4 0 1 2 3 \n4 7 6 5 4\n4 0 4 5 1 \n4 1 5 6 2 \n4 2 6 7 3 \n4 3 7 4 0 \n";

    @BeforeEach
    public void resetModel() throws NumberFormatException, IOException, NotPlyFileException, FacesArrayIsFullException,
            ColorReadException {
        data = reader.ui(model);
    }

    @Test
    public void testGetMinY() {
        assertEquals(data.getMinY(), 0.0);
    }

    @Test
    public void testGetMinX() {
        assertEquals(data.getMinX(), 0.0);
    }

    @Test
    public void testGetMinZ() {
        assertEquals(data.getMinZ(), 0.0);
    }

    @Test
    public void testGetMaxX() {
        assertEquals(data.getMaxX(), 100);
    }

    @Test
    public void testGetMaxY() {
        assertEquals(data.getMaxY(), 100);
    }

    @Test
    public void testGetMaxZ() {
        assertEquals(data.getMaxZ(), 100);
    }

    @Test
    public void testCenterX() {
        assertEquals(data.getCenterX(), 50);
    }

    @Test
    public void testCenterY() {
        assertEquals(data.getCenterY(), 50);
    }

    @Test
    public void testCenterZ() {
        assertEquals(data.getCenterZ(), 50);
    }

    @Test
    public void testColor() {
        assertEquals(data.getFaces()[2].getBlue(), 0);
        data.setFaceColor(2, new int[] { 0, 0, 255 });
        assertEquals(data.getFaces()[2].getBlue(), 255);
    }

    @Test
    public void testCoupe() {
        assertEquals(data.getCoupe(50).size(), 8);
    }

}
