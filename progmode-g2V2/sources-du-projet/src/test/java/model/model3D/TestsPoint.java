package model.model3D;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;


import model.model3D.polygone.Point;

public class TestsPoint {
	Point p1;
	
	@BeforeEach
	public void init() {
		p1 = new Point(25, 50, 100);
	}

	@Test
	public void testTranslationPoint() {
		init();
		p1.translation(-25, -50, 0);
		Point p2 = new Point(0, 0, 100);
		assertEquals(p1.getX(), p2.getX());
		assertEquals(p1.getY(), p2.getY());
		p2.translation(80, 60, -18);
		Point p3 = new Point(80, 60, 82);
		assertEquals(p2.getZ(), p3.getZ());
	}

	@Test
	public void testRotationXPoint() {
		init();
		double y = p1.getY();
		double z = p1.getZ();
		p1.rotate(Math.PI / 2, 0, 0);
		Point p2 = new Point(25, (Math.cos(Math.PI / 2) * y) + (-Math.sin(Math.PI / 2) * z),
				(Math.sin(Math.PI / 2) * y) + (Math.cos(Math.PI / 2) * z));
		assertEquals(p1.getX(), p2.getX());
		assertEquals(p1.getY(), p2.getY());
		assertEquals(p1.getZ(), p2.getZ());
	}

	@Test
	public void testRotationYPoint() {
		init();
		double x = p1.getX();
		double z = p1.getZ();
		p1.rotate(0, Math.PI / 2, 0);
		Point p2 = new Point((Math.cos(Math.PI / 2) * x) - (Math.sin(Math.PI / 2) * z), 50,
				(Math.sin(Math.PI / 2) * x) + (Math.cos(Math.PI / 2) * z));
		assertEquals(p1.getX(), p2.getX());
		assertEquals(p1.getY(), p2.getY());
		assertEquals(p1.getZ(), p2.getZ());
	}

	@Test
	public void testRotationZPoint() {
		init();
		double x = p1.getX();
		double y = p1.getY();
		p1.rotate(0, 0, Math.PI / 2);
		Point p2 = new Point((Math.cos(Math.PI / 2) * x) + (Math.sin(Math.PI / 2) * y),
				(-Math.sin(Math.PI / 2) * x) + (Math.cos(Math.PI / 2) * y), 100);
		assertEquals(p1.getX(), p2.getX());
		assertEquals(p1.getY(), p2.getY());
		assertEquals(p1.getZ(), p2.getZ());
	}
	
	
	@Test
	public void testZoomPoint() {
		double x = p1.getX();
		double y = p1.getY();
		double z = p1.getZ();
		p1.zoom(7);
		assertEquals(x*7, p1.getX());
		assertEquals(y*7, p1.getY());
		assertEquals(z*7, p1.getZ());
	}

}
