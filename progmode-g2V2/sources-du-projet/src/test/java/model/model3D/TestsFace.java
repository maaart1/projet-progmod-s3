package model.model3D;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.model3D.polygone.Face;
import model.model3D.polygone.Point;

public class TestsFace {
	Point p1;
	Point p2;
	Point p3;
	Face f;

	@BeforeEach
	public void init() {
		p1 = new Point(25, 50, 100);
		p2 = new Point(10, 20, 30);
		p3 = new Point(5, 3, 9);
		f = new Face(p1, p2, p3);
	}

	@Test
	public void testGetPoint() {
		assertEquals(p2, f.getPoint(1));
	}

	@Test
	public void testGetTabPoint() {
		Point[] points = f.getPoints();
		assertEquals(points[0], p1);
		assertEquals(points[1], p2);
		assertEquals(points[2], p3);
	}

	@Test
	public void testAddPoint() {
		Face face = new Face(3);
		face.addPoint(p1);
		Point[] points = f.getPoints();
		assertEquals(face.getPoints()[0], points[0]);
		face.addPoint(p2);
		points = f.getPoints();
		assertEquals(face.getPoints()[1], points[1]);
		face.addPoint(p3);
		points = f.getPoints();
		assertEquals(face.getPoints()[2], points[2]);
	}

	@Test
	public void testGraysclale() {
		f.setGray();
		assertEquals(f.grayscale, 0.18394180184548975);
	}

}
