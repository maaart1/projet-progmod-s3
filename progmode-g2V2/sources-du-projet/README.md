# Comment Compiler et Executer le projet
## Version Eclipse
### importer le projet sur eclipse
cliquer sur le menu `File>Import...` puis `Existing Maven Project`

Dans `Root Directory` mettre le chemin vers la racine du projet puis selectionner le premier projet

Cliquer ensuite sur `Finish`

### Executer

**Clique droit** sur le projet puis `Run As>Maven Build..`

Dans la fenêtre écrire dans `Goals` entrer:

    javafx:run

Attendre , le programme devrait s'executer dans les dix secondes qui suivent.
## Version Bash
### Prérequis
> Avoir maven installer sur votre machine
*lien de téléchargement[https://maven.apache.org/]*

A la racine du projet, déplacez vous dans le dossier sources-du-projet

```bash
$ cd sources-du-projet/
```
Ensuite lancer le projet maven à l'aide de la commande suivante:

```bash
$ mvn javafx:run
```
## Version jar
### Prérequis
> Avoir un JRE11 ou plus

Simplement double clique sur `progmode-g2.jar` a la racine du projet