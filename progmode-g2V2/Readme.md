# PROJET DE MODELISATION 3D

## Etudiants

1. Thibaut Martin
2. Corre Léonard
3. Dutoit Hugo

## Preview

Notre application permet d'afficher un fichier ".ply" d'effectuer des translation/rotation/zoom via des bouttons ou la souris. Vous pouvez aussi changer la couleur des files ainsi que celle des faces.
![Preview](https://gitlab.univ-lille.fr/hugo.dutoit.etu/progmode-g2/-/raw/master/captures-ecran/7thRenderInterface.png)

## Répartition des tâches livrable 1

### **_Thibaut Martin_**

> L'Interface, les Tests

### **_Corre Léonard_**

> Lecture ply, Calculs Matriciels

### **_Dutoit Hugo_**

> Affichage Model, Calculs Matriciels

**_La Répartition des tâches n'est pas forcement représentative du travail de chacun car nous avons effectué du pair programming_**

## Problème

1. Création du projet Maven
   > Difficulté lors de l'installation de maven sur nos machine

> Lors de la creation du projet difficultés a appréhender les concepts fondamentaux du fichier pom.xml pourtant essenciel lors de la création d'un projet maven 2. Principe de Branch avec Git
> Nous avons essayer de créer des branch pour travaillé chacun de notre coté mais cela n'a fais que retardé le moment où les conflits apparaissaient et cette fois ci en bien plus grand nombre.

## Répartition des tâches livrable 2

### **_Thibaut Martin_**

> La rotation automatique, TableView, Tests, Couleur, javadoc

### **_Corre Léonard_**

> L'eclairage, Tests, Vue en tranche

### **_Dutoit Hugo_**

> Vue en tranche, PMD, Clean Code, Optimisation

**_La Répartition des tâches n'est pas forcement représentative du travail de chacun car nous avons effectué du pair programming_**

## Problème

> Plus de problème avec Git !

> Un problème pas vraiment technique mais nous avons pris trop de temps a nous y mettre, et n'avons pas eu le temps d'implementer toutes les fonctionnalités que nous pensions développer. Des problèmes de lag du au sortbyAverageZ (maintenant réglés). La qualité de notre code a fait que pour respecter nos règles pmd nous avons du changer une bonne partie de notre code

## Patrons de conception

> le MVC

> NullObject pour notre Model3D

> le Reader est une factory de Model3D
